package br.com.mensagem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * A classe MensagemApplication é responsável por executar o projeto, sendo assin, ela é a base da configuração do sistema.
 * Colocando esta classe em um pacote superior aos demais pacotes do projeto o contexto do spring mapeia tudo a partir deste pacote.
 *
 * Exemplo:
 *
 * |-br
 * |---com
 * |-----mensagem
 * |-------MensagemApplication.java <-- qualquer pacote ou classe a partir deste ponto será lido e carregado no contexto do spring.
 * |-------controller
 * |-------model
 * |-------repository
 * |-------service
 */
@SpringBootApplication
public class MensagemApplication {

    public static void main(String[] args) {
    	SpringApplication.run(MensagemApplication.class, args);
    }
}
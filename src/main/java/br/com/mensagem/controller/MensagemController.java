package br.com.mensagem.controller;

import br.com.mensagem.model.Mensagem;
import br.com.mensagem.service.MensagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Mapeamento do controller para requisições "/mensagem".
 * <p>
 * É mais simples mapear com o @RequestMapping a classe, assim os métodos são responsáveis apenas por mapear o restante do path.
 */
@Controller
@RequestMapping("/mensagem")
public class MensagemController {

    @Autowired
    private MensagemService service;

    /**
     * Insere no Model o atributo necessário para renderizar e trabalhar nos formulários html.
     *
     * @return uma instâcia de Mensagem.
     */
    @ModelAttribute("mensagem")
    public Mensagem modelAttribute() {
        return new Mensagem();
    }

    /**
     * Processa requisições do tipo HTTP Get para o path /mensagem e executa uma busca com base nos dados fornecidos.
     *
     * @param mensagem a Mensagem preenchida a ser utilizada como base para a busca.
     * @param model    o Model Spring para que o resultado da busca seja repassado para a view.
     * @return A view (html) a ser exibido após a busca.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String search(@ModelAttribute("mensagem") Mensagem mensagem, Model model) {
        model.addAttribute("resultadoConsulta", service.search(mensagem));
        return "mensagem/searchPage";
    }

    /**
     * Processa requisições do tipo HTTP Get para o path /mensagem/create e popula qualquer informação necessária para renderizar o formulário.
     *
     * @param model o Model Spring para que os dados necessários no formulário sejam repassados para a view.
     * @return A view (html) a ser exibida.
     */
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createPage(Model model) {

        return "mensagem/formPage";
    }

    /**
     * Processa requisições do tipo HTTP Post para o path /mensagem/save.
     *
     * @param mensagem a Mensagem preenchida a ser salva.
     * @param model    o Model Spring.
     * @return A view (html) a ser exibido.
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String create(@ModelAttribute("mensagem") Mensagem mensagem, Model model) {
        if (mensagem != null) {
            mensagem = service.save(mensagem);
        }
        return search(mensagem, model);
    }


}
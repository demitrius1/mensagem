package br.com.mensagem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.mensagem.model.Mensagem;

/**
 * Repositório de mensagens.
 */
@Repository
public interface MensagemRepository extends JpaRepository<Mensagem, Long> {
}
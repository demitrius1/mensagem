package br.com.mensagem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.mensagem.model.Mensagem;
import br.com.mensagem.repository.MensagemRepository;

import java.util.List;

/**
 * @author Demitrius Medeiros
 */
@Service
public class MensagemService {

    @Autowired
    private MensagemRepository repository;

    public MensagemRepository getRepository() {
        return repository;
    }

    public void setRepository(MensagemRepository repository) {
        this.repository = repository;
    }

    public List<Mensagem> search(Mensagem mensagem) {
        return repository.findAll();
    }

    public Mensagem create(Mensagem mensagem) {
        mensagem = repository.saveAndFlush(mensagem);
        return mensagem;
    }

    public Mensagem save(Mensagem mensagem) {
        return repository.saveAndFlush(mensagem);
    }
}